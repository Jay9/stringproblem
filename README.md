## This Repo contains solution for the following Problems

**Problem 1:**  
Given a string _s_, find the longest palindromic substring in _s_. You may assume that the maximum
length of _s_ is 1000.  

**Solution?:**  
Write a Spring Boot micro-service that completes the following:
1. Receives string input over an API
2. Stores the longest palindrome in provided database
3. Retrieves the stored palindrome over an API
   You should be able to run your micro-service locally and use an API client (such as PostMan) to test your
   results.
   
POST: URL:PORT/strings/lsp
```json
{
  "string": "abcca"
}
```
Sends a request to store the string into the DB with the longest Substring Palindrome (LSP) Generated.  
_Returns_ the given string.
If duplicate value is passed again, it returns a Bad request(400) response.
```
abcca
```

GET: URL:PORT/strings/lsp
```json
{
  "string": "ABCCA"
}
```
_Returns_ a json response with the base string and the lsp.
```json
{
  "base": "ABCCA",
  "lsp": "CC"
}
```
If no data is fount it returns a Resource Not Found (404) response.

**Problem 2/Solution?**:  
Have the function BinaryReversal(str) take the str parameter being passed, which will be a positive
integer, take its binary representation (padded to the nearest N * 8 bits), reverse that string of bits, and
then finally return the new reversed string in decimal form.

For example: if str is "47"; then the binary
version of this integer is 101111, but we pad it to be 00101111. Your program should reverse this binary
string which then becomes: 11110100 and then finally return the decimal version of this string, which is 244.

GET: URL:PORT/strings/binrev/{number}  
Returns a number with the given condition.  

For Example:   
GET URL:PORT/strings/binrev/47 returns 
```
244
```

##NOTE:
This repo may be missing many  
There could be abstract Classes for services, some testing and other things that are missing.
This has been a detail that was thought by the dev to save time and just focus on getting this done.
