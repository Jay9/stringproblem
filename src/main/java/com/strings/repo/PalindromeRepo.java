package com.strings.repo;

import com.strings.entity.Palindrome;
import org.springframework.data.repository.CrudRepository;

public interface PalindromeRepo extends CrudRepository<Palindrome, String> {
}
