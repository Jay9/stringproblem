package com.strings.controller;

public class LSPRequestBody {
    private
        String string;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public String toString() {
        return "LSPRequestBody{" +
                "string='" + string + '\'' +
                '}';
    }
}
