package com.strings.controller;

import com.strings.entity.Palindrome;
import com.strings.exception.BadRequestException;
import com.strings.service.BinaryReversalImpl;
import com.strings.service.PalindromeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.awt.*;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/strings")
public class Strings {

    @Autowired
    BinaryReversalImpl binaryService;

    @Autowired
    PalindromeImpl PalindromeService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String Base() {
        return "/lsp for longest Palindrome of string\n/binrev for binary reversal of the given string (positive number)";
    }

    @GetMapping(value="/lsp", consumes = MediaType.APPLICATION_JSON_VALUE, produces =  MediaType.APPLICATION_JSON_VALUE)
    public Palindrome findPalindrome(@RequestBody LSPRequestBody request) {
        return PalindromeService.findLongSubstrPalindrome(request.getString());
    }

    @PostMapping(value="/lsp", consumes = MediaType.APPLICATION_JSON_VALUE, produces =  MediaType.APPLICATION_JSON_VALUE)
    public String storePalindrome(@RequestBody LSPRequestBody request) {
        return PalindromeService.storeStringPalindrome(request.getString());
    }

    @GetMapping(value = "/binrev/{number}", produces =  MediaType.APPLICATION_JSON_VALUE)
    public int binaryReversal(@PathVariable String number) {
        return binaryService.getBinaryReversal(number);
    }
}
