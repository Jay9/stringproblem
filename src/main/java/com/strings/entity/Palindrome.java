package com.strings.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Palindrome {
    @Id
    @Column(columnDefinition = "VARCHAR(36)", unique = true)
    String base;

    @Column(columnDefinition = "VARCHAR(36)")
    String lsp;

    public Palindrome() {
    }

    public Palindrome(String base) {
        this.base = base;
        this.lsp = generateLongSubPal(base);
    }

    public String getLsp() {
        return lsp;
    }

    public void setLsp(String change) {
        this.lsp = change;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    private static String generateLongSubPal(String base) {
        int maxLength = 1;

        int start = 0;
        int len = base.length();

        int low, high;

        // Consider each char as midpoint
        for (int i = 1; i < len; ++i) {

            // Even lengths __
            low = i - 1;
            high = i;
            while (low >= 0 && high < len
                    && base.charAt(low)
                    == base.charAt(high)) {
                if (high - low + 1 > maxLength) {
                    start = low;
                    maxLength = high - low + 1;
                }
                --low;
                ++high;
            }

            //Odd lengths _^_, i is the pivot
            low = i - 1;
            high = i + 1;
            while (low >= 0 && high < len
                    && base.charAt(low)
                    == base.charAt(high)) {
                if (high - low + 1 > maxLength) {
                    start = low;
                    maxLength = high - low + 1;
                }
                --low;
                ++high;
            }
        }

        return base.substring(start, start + maxLength);
    }


    @Override
    public String toString() {
        return "Palindrome{" +
                "base='" + base + '\'' +
                ", change='" + lsp + '\'' +
                '}';
    }
}
