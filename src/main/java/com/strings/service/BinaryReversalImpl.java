package com.strings.service;

import com.strings.exception.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public class BinaryReversalImpl {
    public int getBinaryReversal(String number) {
        try {
            int num = Integer.parseInt(number, 10);

            if (num < 0) {
                throw new BadRequestException("Cannot use negative numbers in binary reversal");
            }

            String binary = strToBinary(num);
            var reversePadded = new StringBuilder("0".repeat(8-binary.length()) + binary);
            reversePadded.reverse();
            return binToInt(reversePadded.toString());
        }catch(NumberFormatException nEx) {
            throw new BadRequestException("Expected the param to be a string for positive number");
        }catch (BadRequestException b) {
            // forward it to the user
            throw b;
        }
    }

    static String strToBinary(int number) {
        // Custom method to allow for other conversions, using inbuilt method in this case
        return Integer.toBinaryString(number);
    }

    static int binToInt(String binary) {
        // Custom method to allow for other conversion if need be, using inbuilt method in this case
        return Integer.parseInt(binary, 2);
    }
}
