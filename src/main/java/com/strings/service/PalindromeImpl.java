package com.strings.service;

import com.strings.entity.Palindrome;
import com.strings.exception.BadRequestException;
import com.strings.exception.ResourceNotFoundException;
import com.strings.repo.PalindromeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PalindromeImpl {

    @Autowired
    PalindromeRepo repo;

    public Palindrome findLongSubstrPalindrome(String base) {
        return repo.findById(base).orElseThrow(
                () -> new ResourceNotFoundException("No palindrome values found in DB for string " + base)
        );
    }

    public String storeStringPalindrome(String base) {
        if (repo.findById(base).isPresent()) {
            throw new BadRequestException("Palindrome with string: " + base +" already in the database");
        }

        if(base.isEmpty()) {
            throw new BadRequestException("Cannot create palindrome with empty string");
        }

        var p = repo.save(new Palindrome(base));
        return p.getBase();
    }
}
